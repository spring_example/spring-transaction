package com.dzung.transaction;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.dzung.transaction.repository.ScoreRepository;

/**
 * The Class AddScoreServiceTest.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class AddScoreServiceTest {

	/** The score repository. */
	@Autowired
	private ScoreRepository scoreRepository;

	/**
	 * Adds the scores.
	 */
	@Autowired
	private AddScoreService addScoreService;

	/**
	 * Test add scores.
	 */
	@Test
	void testAddScores() {
		int totalRecords = scoreRepository.findAll().size();
		try {
			addScoreService.addScores();
		} catch (Exception e) {
			//
		}
		assertEquals(totalRecords + 1, scoreRepository.findAll().size());
		assertEquals(9, scoreRepository.findTopByOrderByIdDesc().getNumber());
	}

}
