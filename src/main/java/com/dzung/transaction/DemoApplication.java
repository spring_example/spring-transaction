package com.dzung.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * The Class DemoApplication.
 *
 * @author dphungduy
 *
 */
@SpringBootApplication
public class DemoApplication {

	/**
	 * Adds the scores.
	 */
	@Autowired
	private AddScoreService addScoreService;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(final String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	/**
	 * Command line runner.
	 *
	 * @param ctx the ctx
	 * @return the command line runner
	 */
	// @Bean
	public CommandLineRunner commandLineRunner(final ApplicationContext ctx) {
		return args -> {
			try {
				addScoreService.addScores();
			} catch (Exception e) {
				// just for test
			}
		};
	}
}
