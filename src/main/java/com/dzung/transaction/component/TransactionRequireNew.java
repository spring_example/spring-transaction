package com.dzung.transaction.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dzung.transaction.domain.Score;
import com.dzung.transaction.repository.ScoreRepository;

/**
 * The Class TransactionRequireNew.
 *
 * @author dphungduy
 */
@Component
public class TransactionRequireNew {

	/** The score repository. */
	@Autowired
	ScoreRepository scoreRepository;

	/**
	 * Adds the new score.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addNewScore() {
		Score score = new Score();
		score.setNumber(9);
		scoreRepository.save(score);
	}
}
