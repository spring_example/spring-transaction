package com.dzung.transaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dzung.transaction.domain.Score;

/**
 * The Interface ScoreRepository.
 *
 * @author dphungduy
 */
@Repository
public interface ScoreRepository extends JpaRepository<Score, Long> {

	/**
	 * Find top.
	 *
	 * @return the integer
	 */
	Score findTopByOrderByIdDesc();
}
