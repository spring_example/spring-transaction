/**
 *
 */
package com.dzung.transaction;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dzung.transaction.component.TransactionRequireNew;
import com.dzung.transaction.domain.Score;
import com.dzung.transaction.repository.ScoreRepository;

/**
 * The Class AddScoreService.
 *
 * @author dphungduy
 */
@Service
public class AddScoreService {

	/** The transaction require new. */
	@Autowired
	private TransactionRequireNew transactionRequireNew;

	/** The score repository. */
	@Autowired
	private ScoreRepository scoreRepository;

	/**
	 * Adds the scores.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Transactional(rollbackFor = IOException.class)
	public void addScores() throws IOException {
		addScoreWithoutTransactionRequireNew();
		addScoreWithTransactionRequireNewInSameBean();
		transactionRequireNew.addNewScore();
		throw new IOException("test");
	}

	/**
	 * Adds the score without transaction require new.
	 */
	@Transactional
	public void addScoreWithoutTransactionRequireNew() {
		Score score = new Score();
		score.setNumber(3);
		scoreRepository.save(score);
	}

	/**
	 * Adds the score with transaction require new in same bean.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addScoreWithTransactionRequireNewInSameBean() {
		Score score = new Score();
		score.setNumber(6);
		scoreRepository.save(score);
	}
}
